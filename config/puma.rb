threads_count = ENV.fetch('RAILS_MAX_THREADS') { 1 }
threads     threads_count, threads_count
workers     ENV.fetch('WEB_CONCURRENCY') { 4 }
port        ENV.fetch('PORT') { 3000 }
environment ENV.fetch('RAILS_ENV') { 'development' }

preload_app!

on_worker_boot do
  ActiveSupport.on_load(:active_record) do
    ActiveRecord::Base.establish_connection
  end
end

plugin :tmp_restart
