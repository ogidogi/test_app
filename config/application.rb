require_relative 'boot'

require 'rails'
require 'active_model/railtie'
# require 'active_job/railtie'
require 'active_record/railtie'
# require 'active_storage/engine'
require 'action_controller/railtie'
# require 'action_mailer/railtie'
# require 'action_view/railtie'
# require 'action_cable/engine'
# require 'sprockets/railtie'
# require 'rails/test_unit/railtie'

Bundler.require(*Rails.groups)

module TestApp
  class Application < Rails::Application
    config.load_defaults 5.2

    config.api_only = true

    Oj.optimize_rails

    config.middleware.delete Rack::Sendfile
    config.middleware.delete Rack::Runtime
    config.middleware.delete Rack::Head
    config.middleware.delete Rack::ConditionalGet
    config.middleware.delete Rack::ETag
  end
end
