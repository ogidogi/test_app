Rails.application.routes.draw do
  resources :posts, only: :create do
    post :rate, on: :member
    get :top, on: :collection
    get :authors, on: :collection
  end

  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'
end
