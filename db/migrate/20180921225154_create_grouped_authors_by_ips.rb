class CreateGroupedAuthorsByIps < ActiveRecord::Migration[5.2]
  def change
    create_view :grouped_authors_by_ips, materialized: true

    add_index :grouped_authors_by_ips, :author_ip, unique: true
  end
end
