class CreatePostSearchResults < ActiveRecord::Migration[5.2]
  def change
    create_view :post_search_results, materialized: true

    add_index :post_search_results, :post_id, unique: true
  end
end
