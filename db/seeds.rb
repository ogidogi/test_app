if Rails.env.development?
  ip_list = File.read(Rails.root.join('db', 'seed_data', 'ip_list.txt')).split("\n")
  user_names = File.read(Rails.root.join('db', 'seed_data', 'authors.txt')).split("\n")
  rating = [1, 2, 3, 4, 5]

  puts 'Clear db'
  User.delete_all
  Post.delete_all
  Rating.delete_all

  puts 'Create users'
  users = user_names.map { |u| "('#{u}')" }.join(', ')
  ActiveRecord::Base.connection.execute("INSERT INTO users VALUES #{users};")

  puts 'Create posts'
  post_ids = 200.times.map do
    posts = 1000.times.map do
      "('test', 'test', '#{user_names.sample}', '#{ip_list.sample}')"
    end.join(', ')
    ActiveRecord::Base.connection.execute("INSERT INTO posts (title, content, author_id, author_ip) VALUES #{posts} returning id;")
  end

  puts 'Create ratings'
  post_ids.map do |p_ids|
    ratings = p_ids.map do |post|
      10.times.map { "('#{post['id']}', '#{rating.sample}')" }.join(', ')
    end.join(', ')
    ActiveRecord::Base.connection.execute("INSERT INTO ratings (post_id, value) VALUES #{ratings};")
  end

  PostSearchResult.refresh
  GroupedAuthorsByIp.refresh
end
