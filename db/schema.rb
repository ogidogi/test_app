# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_09_21_225154) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "posts", force: :cascade do |t|
    t.string "title"
    t.text "content"
    t.string "author_ip"
    t.string "author_id"
    t.index ["author_id"], name: "index_posts_on_author_id"
  end

  create_table "ratings", force: :cascade do |t|
    t.integer "value"
    t.bigint "post_id"
    t.index ["post_id"], name: "index_ratings_on_post_id"
  end

  create_table "users", primary_key: "login", id: :string, force: :cascade do |t|
  end


  create_view "post_search_results", materialized: true,  sql_definition: <<-SQL
      WITH posts_ratings AS (
           SELECT ratings.post_id,
              avg(ratings.value) AS rating
             FROM ratings
            GROUP BY ratings.post_id
          )
   SELECT posts.id AS post_id,
      posts.title,
      posts.content,
      posts.author_id,
      posts.author_ip,
      posts_ratings.rating
     FROM posts,
      posts_ratings
    WHERE (posts.id = posts_ratings.post_id)
    ORDER BY posts_ratings.rating DESC;
  SQL

  add_index "post_search_results", ["post_id"], name: "index_post_search_results_on_post_id", unique: true

  create_view "grouped_authors_by_ips", materialized: true,  sql_definition: <<-SQL
      SELECT posts.author_ip,
      array_agg(DISTINCT posts.author_id) AS authors
     FROM posts
    GROUP BY posts.author_ip;
  SQL

  add_index "grouped_authors_by_ips", ["author_ip"], name: "index_grouped_authors_by_ips_on_author_ip", unique: true

end
