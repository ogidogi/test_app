WITH posts_ratings as (
  SELECT ratings.post_id, AVG(ratings.value) as rating
  FROM ratings
  GROUP BY ratings.post_id
)
SELECT
  posts.id as post_id,
  posts.title,
  posts.content,
  posts.author_id,
  posts.author_ip,
  posts_ratings.rating
FROM
  posts, posts_ratings
WHERE
  posts.id = posts_ratings.post_id
ORDER BY
  posts_ratings.rating DESC;
