SELECT
  author_ip,
  array_agg(DISTINCT author_id) authors
FROM posts
GROUP BY posts.author_ip;
