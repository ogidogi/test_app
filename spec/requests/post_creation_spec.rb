require 'rails_helper'

describe 'Post creation', type: :request do
  context 'with valid params' do
    let(:params) {
      {
        title: 'test',
        content: 'test',
        author_ip: '127.0.0.1',
        login: 'test_user'
      }
    }
    let(:expected_response) {
      {
        title: 'test',
        content: 'test',
        author_ip: '127.0.0.1',
        author_id: 'test_user'
      }
    }

    it 'creates new post' do
      expect { post '/posts', params: params }.to change(Post, :count).by(1)
    end

    it 'creates new user' do
      expect { post '/posts', params: params }.to change(User, :count).by(1)
    end

    it 'returns valid response' do
      post '/posts', params: params

      expect(response).to have_http_status(:ok)
      json_response = JSON.parse(response.body, symbolize_names: true)
      expect(json_response).to have_key(:id)
      expect(json_response).to include(expected_response)
    end

    context 'user exists' do
      let!(:user) { create(:user, login: params[:login]) }

      it 'not create new user' do
        expect { post '/posts', params: params }.not_to change(User, :count)
      end
    end
  end

  context 'with invalid params' do
    let(:params) {
      {
        title: nil,
        content: 'test',
        author_ip: :invalid,
        login: 'test_user'
      }
    }
    let(:expected_response) {
      { errors: ['Title can\'t be blank', 'Author ip is invalid'] }
    }

    it 'returns validation errors' do
      post '/posts', params: params

      expect(response).to have_http_status(:unprocessable_entity)
      json_response = JSON.parse(response.body, symbolize_names: true)
      expect(json_response).to eq(expected_response)
    end
  end
end
