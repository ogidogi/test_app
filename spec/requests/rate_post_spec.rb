require 'rails_helper'

describe 'Rate post', type: :request do
  let(:author) { create(:user) }
  let!(:article) { create(:post, author: author) }

  let(:value) { 3 }

  it 'creates rating for post' do
    expect {
      post rate_post_path(article.id), params: { value: value }
    }.to change(Rating, :count).by(1)
  end

  context 'invalid rating value' do
    let(:value) { 0 }
    let(:expected_response) {
      {  errors: ['Value is not included in the list'] }
    }

    it 'returns error response' do
      post rate_post_path(article.id), params: { value: value }

      json_response = JSON.parse(response.body, symbolize_names: true)
      expect(json_response).to eq(expected_response)
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end
end
