require 'rails_helper'

describe RatePostService do
  let(:value) { 4 }
  let(:post) { create(:post, author: create(:user)) }
  let(:params) { { post: post, value: value } }
  let(:service) { RatePostService.new(params) }

  it 'creates new rating record for post' do
    expect(service).to receive(:refresh_post_search_view)
    service.rate_post

    expect(service.success?).to eq(true)
    expect(service.rating).to be_persisted
  end

  context 'invalid rate value' do
    let(:value) { -1 }

    it 'rating for post is not created' do
      expect { service.rate_post }.not_to change(Rating, :count)
    end

    it 'set error result' do
      expect(service).not_to receive(:refresh_post_search_view)
      service.rate_post

      expect(service.success?).to eq(false)
      expect(service.errors).to be_present
      expect(service.rating).to be_nil
    end
  end
end
