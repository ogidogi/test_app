require 'rails_helper'

describe CreatePostService do
  let(:service) { CreatePostService.new(params) }

  context 'valid form params' do
    let(:params) { { title: 'test', content: 'test', login: 'login', author_ip: '127.0.0.1' } }

    before do
      expect(service).to receive(:refresh_grouped_authors_view)
    end

    describe '#create' do
      context 'author do not exists' do
        it 'creates new user' do
          expect { service.create }.to change(User, :count).by(1)
        end

        it 'creates new post' do
          expect { service.create }.to change(Post, :count).by(1)
        end
      end

      context 'author exists' do
        before do
          create(:user, login: params[:login])
        end

        it 'not create new user' do
          expect { service.create }.not_to change(User, :count)
        end

        it 'creates new post' do
          expect { service.create }.to change(Post, :count).by(1)
        end
      end
    end
  end

  context 'invalid form params' do
    let(:params) { { title: 'test', author_ip: :invalid } }

    before do
      expect(service).not_to receive(:refresh_grouped_authors_view)
    end

    describe '#create' do
      it 'not creates new post' do
        expect { service.create }.not_to change(Post, :count)
      end
    end
  end
end
