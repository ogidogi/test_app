FactoryBot.define do
  factory :post do
    title { 'title' }
    content { 'content' }
    author_ip { Faker::Internet.ip_v4_address }
    author_id { Faker::Name.unique.name }

    trait :with_errors do
      title { nil }
      author_ip { 'invalid' }
    end
  end
end
