FactoryBot.define do
  factory :rating do
    value { [1, 2, 3, 4, 5].sample }
    association :post
  end
end
