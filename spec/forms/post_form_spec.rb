require 'rails_helper'

describe PostForm, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:content) }
    it { should validate_presence_of(:login) }
    it { should allow_value('127.0.0.1').for(:author_ip) }
  end
end
