class PostsController < ApplicationController
  def create
    post_creator = CreatePostService.new(post_form_params)
    post_creator.create

    if post_creator.success?
      render json: post_creator.post.as_json(only: [:id, :title, :content, :author_id, :author_ip])
    else
      render json: { errors: post_creator.errors }, status: :unprocessable_entity
    end
  end

  def rate
    post = Post.find(params[:id])
    post_rater = RatePostService.new(post: post, value: params[:value])
    post_rater.rate_post

    if post_rater.success?
      render json: { rating: post.average_rating }
    else
      render json: { errors: post_rater.errors }, status: :unprocessable_entity
    end
  end

  def top
    posts = PostSearchResult.limit(params[:n] || 20)

    render json: posts.as_json(only: [:title, :content])
  end

  def authors
    result = GroupedAuthorsByIp.all

    render json: result.as_json(only: [:author_ip, :authors])
  end

  private

  def post_form_params
    params.permit(:title, :content, :login, :author_ip)
  end
end
