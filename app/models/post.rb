class Post < ApplicationRecord
  belongs_to :author, class_name: User.name, primary_key: :login
  has_many :ratings, dependent: :destroy
  has_one :post_search_result

  delegate :rating, to: :post_search_result, prefix: :average, allow_nil: true
end
