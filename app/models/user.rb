class User < ApplicationRecord
  self.primary_key = :login

  has_many :posts, dependent: :destroy, foreign_key: :author_id
end
