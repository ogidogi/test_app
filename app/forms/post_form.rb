class PostForm
  include ActiveModel::Model

  attr_accessor :title, :content, :login, :author_ip

  validates :title, :content, :login, presence: true
  validates :author_ip, format: { with: Resolv::IPv4::Regex }

  def to_create_params
    {
      title: title,
      content: content,
      author_ip: author_ip,
      author_id: login
    }
  end
end
