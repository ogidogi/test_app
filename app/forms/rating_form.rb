class RatingForm
  include ActiveModel::Model

  attr_accessor :post, :value

  validates :value, inclusion: { in: 1..5 }

  def to_create_params
    {
      post_id: post.id,
      value: value
    }
  end
end
