class RefreshViewWorker
  include Sidekiq::Worker
  sidekiq_options retry: false,
                  lock:  :until_executed

  def perform(view_class)
    view_class.constantize.refresh
  end
end
