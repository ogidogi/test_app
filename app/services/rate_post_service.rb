class RatePostService
  attr_reader :post, :value
  attr_reader :rating, :errors

  def initialize(post:, value:)
    @post = post
    @value = value.to_i
  end

  def rate_post
    if rating_form.valid?
      create_rating
      refresh_post_search_view
    else
      build_errors
    end
  end

  def success?
    rating_form.errors.empty?
  end

  private

  def rating_form
    @rating_form ||= RatingForm.new(post: post, value: value)
  end

  def create_rating
    @rating = Rating.create(rating_form.to_create_params)
  end

  def refresh_post_search_view
    RefreshViewWorker.perform_async(PostSearchResult.name)
  end

  def build_errors
    @errors = rating_form.errors.full_messages
  end
end
