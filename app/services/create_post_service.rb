class CreatePostService
  attr_reader :params
  attr_reader :post, :errors

  def initialize(params)
    @params = params
  end

  def create
    if post_form.valid?
      create_user_if_not_exist
      create_post
      refresh_grouped_authors_view
    else
      build_errors
    end
  end

  def success?
    errors.blank?
  end

  private

  def post_form
    @post_form ||= PostForm.new(params)
  end

  def create_user_if_not_exist
    unless User.exists?(login: post_form.login)
      User.create(login: post_form.login)
    end
  end

  def create_post
    @post = Post.create(post_form.to_create_params)
  end

  def refresh_grouped_authors_view
    RefreshViewWorker.perform_async(GroupedAuthorsByIp.name)
  end

  def build_errors
    @errors = post_form.errors.full_messages
  end
end
